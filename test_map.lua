
local Map = require("lib.Map")

function printExport(export)
  local rows = 0
  for k,v in pairs(export) do
    rows = rows + 1
  end
  for z = 0, rows - 1 do
    print(export[z])
  end
end

local map = Map:new(16, 16)

map:set(8, 10, 'X')
map:set(9, 9, "X")
map:set(10, 8, "X")

map:calculatePath(5, 5, 14, 14)

print("Pathfind:")
printExport(map:exportPath(), 16)

local export = map:export()

print("Export:")
printExport(export)

map:import(export)
export = map:export()
print("Import/Re-Export:")
printExport(export)