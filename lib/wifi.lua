
local component = require("component")

local wifi = {}

wifi.LOG_PORT = 32001
wifi.DEFAULT_STRENGTH = 200

-- Returns the best available wifi component
local function findWifiModem()
  for address, name in component.list("modem") do
    if name == "modem" then
      local modem = component.proxy(address)
      if modem.isWireless() then
        modem.setStrength(wifi.DEFAULT_STRENGTH)
        return modem
      end
    end
  end
  return nil
end

wifi.modem = findWifiModem()

function wifi.sendMessage(port, message, strength)
  if not wifi.modem then
    return
  end
  strength = strength or wifi.DEFAULT_STRENGTH
  wifi.modem.setStrength(strength)
  wifi.modem.broadcast(port, message)
end

function wifi.hasModem()
  return not wifi.modem == nil
end

return wifi