-- Compound inventory routines'
local robot = require("robot")

local inventory = {}

function inventory.consolidate(num_reserved)
  local inv_size = robot.inventorySize()
  for i = 0,1 do
    -- Doing two passes works
    for current = num_reserved, inv_size do
      local current_count = robot.count(current)
      local current_space = robot.space(current)
      if current_count > 0 and current_space > 0 then
        for compare = current + 1, inv_size do
          local compare_count = robot.count(compare)
          if compare_count > 0 then
            local transfer_count = compare_count
            if transfer_count > current_space then
              transfer_count = current_space
            end
            robot.select(compare)
            if robot.compareTo(current) then
              robot.transferTo(current, transfer_count)
            end
          end
        end
      end
    end
  end
end

function inventory.findEmpty(num_reserved)
  local inv_size = robot.inventorySize()
  for current = num_reserved, inv_size do
    if robot.count(current) == 0 then
      return current
    end
  end
  return -1
end

return inventory