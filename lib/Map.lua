
local List = require("lib.List")

local MAP_UNKNOWN = '?'
local MAP_CLEARED = ' '
local MAP_BLOCKED = 'X'
local MAP_MAX_COST = 1000000

local Map = {}

Map.UNKNOWN = MAP_UNKNOWN
Map.CLEARED = MAP_CLEARED
Map.BLOCKED = MAP_BLOCKED
Map.MAX_COST = MAP_MAX_COST

function Map:new(x, z)
  local map = {}
  map.x = x
  map.z = z
  map.data = {}
  map.path = {}
  for i = 0, x*z do
    map.data[i] = MAP_UNKNOWN
    map.path[i] = -1
  end
  setmetatable(map, self)
  self.__index = self
  return map
end

function Map:boundsCheck(x, z)
  if x < 0 then return false end
  if x >= self.x then return false end
  if z < 0 then return false end
  if z >= self.z then return false end
  return true
end

function Map:get(x, z)
  if not self:boundsCheck(x, z) then return MAP_BLOCKED end
  return self.data[z*self.x + x]
end

function Map:set(x, z, value)
  if not self:boundsCheck(x, z) then return end
  self.data[z*self.x + x] = value
end

function Map:getPath(x, z)
  if not self:boundsCheck(x, z) then return MAP_MAX_COST end
  return self.path[z*self.x + x]
end

function Map:setPath(x, z, value)
  if not self:boundsCheck(x, z) then return end
  self.path[z*self.x + x] = value
end

function Map:export()
  local export = {}
  for z = 0, self.z - 1 do
    local line = ""
    for x = 0, self.x - 1 do
      line = line .. self:get(x, z)
    end
    export[z] = line
  end
  return export
end

function Map:import(data)
  local rows = 0
  for k,v in ipairs(data) do
    rows = rows + 1
  end
  for z = 0, rows do
    local x = 0
    for c in string.gmatch(data[z], ".") do
      self:set(x, z, c)
      x = x + 1
    end
    z = z + 1
  end
end

function Map:exportPath()
  local export = {}
  for z = 0, self.z - 1 do
    local line = ""
    for x = 0, self.x - 1 do
      line = line .. string.char(97 + self:getPath(x,z))
    end
    export[z] = line
  end
  return export
end

function Map:clearPath()
  for i = 0, self.x*self.z do
    -- -1 on the path means uncalculated
    self.path[i] = -1
  end
end

function Map:calculatePath(from_x, from_z, to_x, to_z)
  local queue = List.new()

  self:clearPath()
  self:setPath(to_x, to_z, 0)
  List.pushRight(queue, { x = to_x, z = to_z })

  while List.size(queue) > 0 do
    local item = List.popLeft(queue)
    local x = item.x
    local z = item.z
    local cost = self:getPath(x,z)
    local new_cost = cost + 1
    local new_x
    local new_z

    -- If we've found a route from the destination to start then exit    
    if x == from_x and z == from_z then
      return
    end

    -- For every adjacent square that's uncalculated and passable, give it a cost and add it to the list
    if x > 0 then
      new_x = x - 1
      new_z = z
      if self:_calculateTile(new_x, new_z, new_cost) then
        List.pushRight(queue, { x = new_x, z = new_z })
      end
    end

    if x < self.x - 1 then
      new_x = x + 1
      new_z = z
      if self:_calculateTile(new_x, new_z, new_cost) then
        List.pushRight(queue, { x = new_x, z = new_z })
      end
    end

    if z > 0 then
      new_x = x
      new_z = z - 1
      if self:_calculateTile(new_x, new_z, new_cost) then
        List.pushRight(queue, { x = new_x, z = new_z })
      end
    end

    if z < self.z - 1 then
      new_x = x
      new_z = z + 1
      if self:_calculateTile(new_x, new_z, new_cost) then
        List.pushRight(queue, { x = new_x, z = new_z })
      end
    end
  end

end

-- Return true if the tile should be added to the queue.
-- This is supposed to be private.
function Map:_calculateTile(x, z, new_cost)
  local prev_cost = self:getPath(x, z)
  if prev_cost ~= -1 and new_cost >= prev_cost then
    return false
  end
  local contents = self:get(x, z)
  if contents == MAP_BLOCKED then
    return false
  end
  self:setPath(x, z, new_cost)
  return true
end

local function manhattanDistance(x1, z1, x2, z2)
  return math.abs(x2 - x1) + math.abs(z2 - z1)
end

-- Returns the direction to the best path value.
function Map:getBestPath(from_x, from_z)
  local best_x = nil
  local best_z = nil
  local best_cost = MAP_MAX_COST
  local cost
  local tests = {
    a = { x =  0, z =  1 },
    b = { x =  0, z = -1 },
    c = { x =  1, z =  0 },
    d = { x = -1, z =  0 }
  }
  
  for k,ofs in pairs(tests) do
      cost = self:getPath(from_x + ofs.x, from_z + ofs.z)
      if cost >= 0 and cost < best_cost then
        best_x = ofs.x
        best_z = ofs.z
        best_cost = cost
      end
  end
  
  return best_x, best_z
end

function Map:findNearestTile(to_x, to_z, tile_type)
  local closest_x = nil
  local closest_z = nil
  local closest_dist = self.x * self.z
  local dist
  for z = 0, self.z - 1 do
    for x = 0, self.x - 1 do
      if self:get(x, z) == tile_type then
        dist = manhattanDistance(x, z, to_x, to_z)
        if dist < closest_dist then
          closest_dist = dist
          closest_x = x
          closest_z = z
        end
      end
    end
  end
  return closest_x, closest_z
end

return Map