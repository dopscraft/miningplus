local computer = require("computer")

local music = {}

local c4 = 261.63

local equalTemperament = {
  ["c"]  = 261.63,
  ["c#"] = 277.18,
  ["db"] = 277.18,
  ["d"]  = 293.66,
  ["d#"] = 311.13,
  ["eb"] = 311.13,
  ["e"]  = 329.63,
  ["fb"] = 329.63,
  ["e#"] = 349.23,
  ["f"]  = 349.23,
  ["f#"] = 369.99,
  ["gb"] = 369.99,
  ["g"]  = 392.00,
  ["g#"] = 415.30,
  ["ab"] = 415.30,
  ["a"]  = 440.00,
  ["a#"] = 466.16,
  ["bb"] = 466.16,
  ["b"]  = 493.88,
  ["cb"] = 493.88
}

local justTemperament = {
  ["c"]  = c4,
  ["c#"] = c4 * 25 / 24,
  ["db"] = c4 * 16 / 15,
  ["d"]  = c4 * 9 / 8,
  ["d#"] = c4 * 75 / 64,
  ["eb"] = c4 * 6 / 5,
  ["e"]  = c4 * 5 / 4,
  ["fb"] = c4 * 32 / 25,
  ["e#"] = c4 * 125 / 96,
  ["f"]  = c4 * 4 / 3,
  ["f#"] = c4 * 25 / 18,
  ["gb"] = c4 * 36 / 25,
  ["g"]  = c4 * 3 / 2,
  ["g#"] = c4 * 25 / 16,
  ["ab"] = c4 * 8 / 5,
  ["a"]  = c4 * 5 / 3,
  ["a#"] = c4 * 125 / 72,
  ["bb"] = c4 * 9 / 5,
  ["b"]  = c4 * 15 / 8,
  ["cb"] = c4 * 48 / 25
}

--
-- Set default octave and things
--
local function createState()
  local state = {
    tick = 0.1,
    duration = 4,
    octave = 4,
    notes = equalTemperament
  }
  return state
end

--
-- Play a segment that's a note
--
local function playNote(note, state)
  -- TODO: shorthand octave on end
  if state.notes[note] == nil then
    return
  end
  local duration = state.duration * state.tick
  if note == "r" then
    os.sleep(duration)
    return
  end
  local freq = state.notes[note] * 2 ^ (state.octave - 4)
  computer.beep(freq, duration)
end

--
-- Handle an individual segment of a string
--
local function parseSegment(seg, state)
  if (string.sub(seg, 1, 1) == "%") then
    local cmd = string.sub(seg, 2, 2)
    local var = string.sub(seg, 3, -1)
    if cmd == "L" then
      state.tick = tonumber(var)
    elseif cmd == "D" then
      state.duration = tonumber(var)
    elseif cmd == "O" then
      state.octave = tonumber(var)
    elseif cmd == "T" then
      if var == "just" then
        state.notes = justTemperament
      elseif var == "equal" then
        state.notes = equalTemperament
      else
        print("unrecognised temperament " .. var)
      end
    else
      print("unrecognised instruction " .. cmd .. "," .. var)
    end
  else
    playNote(seg, state)
  end
end

--
-- Parse a single string
--
local function playString(str, state)
  for seg in string.gmatch(str, "([^;,]+)") do
    parseSegment(seg, state)
  end
end

--
-- Play a tune, either a single string or an array of notes
--
function music.play(tune)
  local state = createState()
  if type(tune) == "string" then
    playString(tune, state)
  elseif type(tune) == "table" then
    for k, v in pairs(tune) do
      playString(v, state)
    end
  end
end

return music