do

  local wifi = require("lib.wifi")

  local Log = {}

  function Log:new(filename, wifi_prefix)
    local log = {}
    log.wifi_prefix = ""
    if wifi_prefix then
      log.wifi_prefix = wifi_prefix .. " "
    end
    if filename ~= nil then
      log.file = io.open(filename .. ".log", w)
    end
    setmetatable(log, self)
    self.__index = self
    return log
  end

  function Log:write(message)
    local prefix = ""
    if self.file then
      self.file.write(message .. "\n")
    end
    if wifi.modem then
      wifi.sendMessage(wifi.LOG_PORT, self.wifi_prefix .. " " .. message)
      prefix = "* "
    end
    print(prefix .. message)
  end
  
  return Log

end 