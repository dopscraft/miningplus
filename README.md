Mining Plus
===========

Mining programs for the OpenComputers mod
for Minecraft.

Excavate
--------

Usage:

    excavate <forward> <right> [<torches>]
    excavate resume

Will excavate a volume forward * right * 3
blocks, of which the robot is in the back left
corner, and in the middle vertically.
I.e. it will dig 1 up, 1 down, forward-1
forward and right-1 right.

The robot will attempt to navigate around any
blocks it can't dig, meaning you can give it
a stone shovel and dig the fancy ores yourself.
This is intended for mods like Thermal Expansion
where you get higher yields on redstone and
diamond if you dig them with silk touch and break
them in a pulverizer.

If the robot can't dig any more blocks, it'll
stop with a reason, which it will also write to
"excavate.log".

It might be possible to resolve the issue that
caused the robot to stop, in which case, using
the command "excavate resume" will let the
robot carry on where it left off. If you change
its position things will go wrong, but you can
move it so long as you put it back where it
was.

The robot will try to place torches every 4
blocks. If it can't do so it will just carry on,
but if it runs out of torches it will halt.


Prospect
--------

Usage:

    prospect <surface_altitude> [ <deepest_altitude> ]

The prospect program makes a robot dig down from
where it was placed until it reaches the instructed
altitude, and then returns to the surface. As it
descends it records how open the shaft is, from 0
(fully enclosed) to 4 (open on all 4 sides).
This information will be written to "prospect.log".

The surface_altitude parameter should be the robot's
current altitude in order for all its reported depths
to be correct.

The default value for deepest_altitude is 0.


Listener
--------

Usage:

    listener
    
If wifi is available, this will use it to listen
for status messages from the robot. It will
display the same information that they write to
their log and display on their built-in screen.

