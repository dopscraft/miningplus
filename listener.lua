local event = require("event")
local wifi = require("lib.wifi")

if not wifi.modem then
  print("No wireless modem found")
  os.exit()
end

print("listening on " .. wifi.LOG_PORT)
wifi.modem.open(wifi.LOG_PORT)
wifi.modem.setStrength(200)

while true do
  local _, _, from, port, _, message = event.pull("modem_message")
  print("[" .. os.date("%H:%M:%S") .. "] " .. message)
end