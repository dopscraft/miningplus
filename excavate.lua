--
-- External includes
--
local shell = require("shell")
local sides = require("sides")
local computer = require("computer")
local robot = require("robot")

--
-- Our own includes
--

local inventory = require("lib.inventory")
local Map = require("lib.Map")
local Log = require("lib.Log")

local log = Log:new("excavate", robot.name())

--
-- Main parameters
--
local args = { ... }
local forward = 0
local right = 0
local torches = 0
local resume = false
local resume_state = {}

--
-- Internal state
--
local relx = 0 -- +x is forward
local rely = 0 -- +y is right
local relang = 0 -- 0 is forward, 1 is right, 2 is backward, 3 is left
local reserved_inventory = 0
local map = nil
local running = true
local targetx = nil
local targety = nil

--
-- Load from parameters
--
local function init()
  if args[1] == nil then
    print("Usage")
    print("  excavate <forward> <right> [ <torches> ]")
    print("  excavate resume")
    os.exit()
  elseif args[1] == "resume" then
    resume = true
    resume_state = loadState()
    forward = resume_state.forward
    right = resume_state.right
    torches = resume_state.torches
    relx = resume_state.relx
    rely = resume_state.rely
    relang = resume_state.relang
  else
    forward = tonumber(args[1] or 0)
    right = tonumber(args[2] or 0)
    torches = tonumber(args[3] or 0)
  end
  log:write("starting: forward=" .. forward .. ", right=" .. right .. ", torches=" .. torches)
  if torches then
    reserved_inventory = 1
  end
  map = Map:new(forward, right)
  if resume then
    map:import(resume_state.map)
  end
end

--
-- Graceful shutdown
--
local function shutdown(message)
  saveState()
  log:write(message)
  log:write("Shutdown")
  computer.shutdown(false)
end

local function halt(message)
  saveState()
  log:write(message)
  log:write("Halt")
  os.exit()
end

local function saveState()
  local file = io.open("resume.state", "w")
  file:write(forward .. "\n")
  file:write(right .. "\n")
  file:write(torches .. "\n")
  file:write(relx .. "\n")
  file:write(rely .. "\n")
  file:write(relang .. "\n")
  local export = map:export()
  for z = 0, right - 1 do
    file:write(export[z] .. "\n")
  end
end

local function loadState()
  local state = {}
  local file = io.open("resume.state", "r")
  state.forward = tonumber(file:read() or 0)
  state.right = tonumber(file:read() or 0)
  state.torches = tonumber(file:read() or 0)
  state.relx = tonumber(file:read() or 0)
  state.rely = tonumber(file:read() or 0)
  state.relang = tonumber(file:read() or 0)
  state.map = {}
  for z = 0, state.right - 1 do
    state.map[z] = file:read() or ""
  end
  file:close()
  return state
end

--
-- Keeps track of our angle while turning
--
local function face(angle)
  local diff = relang - angle
  if diff == 1 or diff == -3 then
    robot.turnLeft()
  elseif diff == 2 or diff == -2 then
    robot.turnAround()
  elseif diff == 3 or diff == -1 then
    robot.turnRight()
  elseif diff ~= 0 then
    halt("Internal error: invalid difference between angles")
  end
  relang = angle
end

local function angleForOffset(ofsx, ofsy)
  if ofsx > 0 then return 0 end
  if ofsy > 0 then return 1 end
  if ofsx < 0 then return 2 end
  if ofsy < 0 then return 3 end
  halt("Internal error: invalid offset")
end

local function offsetForAngle(ang)
  if ang == 0 then return 1, 0 end
  if ang == 1 then return 0, 1 end
  if ang == 2 then return -1, 0 end
  if ang == 3 then return 0, -1 end
  halt("Internal error: invalid angle")
end

--
-- Keeps track of our coordinates while moving
--
local function move()
  if not robot.forward() then
    return false
  end
  local ofsx, ofsy = offsetForAngle(relang)
  relx = relx + ofsx
  rely = rely + ofsy
  return true
end

--
-- Digs or halts if we got full
--
local function dig(side)
  local empty = inventory.findEmpty(reserved_inventory)
  if empty < 0 then
    inventory.consolidate(reserved_inventory)
    empty = inventory.findEmpty(reserved_inventory)
  end
  if empty < 0 then
    halt("Inventory full")
  end
  local dura = robot.durability()
  -- hack to stop digging when durability is less than 1%, for Tinker's Construct
  -- TODO: Take a parameter or use config to specify the tool durability.
  if dura == nil or dura <= 0.01 then
    halt("Tool broken")
  end
  local swung, reason
  if side == sides.top then
    swung, reason = robot.swingUp()
  elseif side == sides.bottom then
    swung, reason = robot.swingDown()
  else
    swung, reason = robot.swing(side)
  end
  if swung or reason == "air" then
    return true
  end
  return false
end

--
-- Other utility stuff
--
local function placeTorch()
  if torches == 0 or relx % 4 ~= 0 or rely % 4 ~= 0 then
    return
  end
  if robot.count(1) < 1 then
    halt("Run out of torches")
  end
  robot.select(1)
  robot.placeDown()
end

--
-- Navigation stuff
-- 
local function ensureTargetExists()
  while targetx == nil do
    -- First get the nearest unexplored tile.
    targetx, targety = map:findNearestTile(relx, rely, Map.UNKNOWN)
    if targetx == nil then
      halt("All targets found or blocked")
    end
  end
end
  
local function tryGetPath()
  local ofsx
  local ofsy
  -- Try to pathfind to it.
  map:calculatePath(relx, rely, targetx, targety)
  -- If we can't pathfind to it, then mark it as blocked.
  -- We'll go round the loop and find another one.
  ofsx, ofsy = map:getBestPath(relx, rely)
  if ofsx ~= nil then
    return ofsx, ofsy
  end
  map:set(targetx, targety, Map.BLOCKED)
  return nil, nil
end

local function tryMove(ofsx, ofsy)
  local angle = angleForOffset(ofsx, ofsy)
  face(angle)
  if not dig(sides.front) then
    map:set(relx + ofsx, rely + ofsy, Map.BLOCKED)
    return false
  end
  if not move() then
    map:set(relx + ofsx, rely + ofsy, Map.BLOCKED)
    return false
  end
  return true
end

--
-- Overall algorithm
--
local function frame()
  if (computer.energy() / computer.maxEnergy()) < 0.05 then
    shutdown("Not enough energy")
  end
    
  if map:get(relx, rely) ~= Map.CLEARED then
    dig(sides.top)
    if dig(sides.bottom) then
      placeTorch()
    end
    map:set(relx, rely, Map.CLEARED)
  end

  -- The bit for getting our next movement direction.
  ensureTargetExists()
  local ofsx, ofsy = tryGetPath()
  if ofsx == nil then
    -- Couldn't pathfind, so we reset our target.
    -- Give up for now, next frame we'll pick a new target.
    targetx = nil
    targety = nil
    return
  end
  
  -- The bit for moving in that direction.
  if not tryMove(ofsx, ofsy) then
    -- Couldn't move there, so we set it to blocked and make us re-pathfind.
    -- If we clear our target totally, then even if we were trying to move into
    -- our target cell then it'll be handled gracefully.
    map:set(relx + ofsx, rely + ofsy, Map.BLOCKED)
    targetx = nil
    targety = nil
    return
  end
end

--
-- The actual program starts!
--

init()

while true do
  frame()
  os.sleep(0.001)
end
