local robot = require("robot")
local computer = require("computer")
local Log = require("lib.Log")
local music - require("lib.music")

local log = Log:new("prospect", robot.name())
local args = { ... }

local surface_depth
local current_depth
local max_depth

local min_energy

local last_scan

--
-- Gather parameters
--
local function init()
  if args[1] == nil then
    print("Usage")
    print("  prospect <current_depth> [ <max_depth> ]")
    os.exit()
  end
  current_depth = tonumber(args[1] or 69)
  max_depth = tonumber(args[2] or 0)
  surface_depth = current_depth
  min_energy = computer.energy() * 0.5
  music.play("%D2,%O5,b")
end

--
-- Returns obstructed state in the cardinal directions
--
local function scan()
  local f = robot.detect()
  robot.turnLeft()
  local l = robot.detect()
  robot.turnLeft()
  local b = robot.detect()
  robot.turnLeft()
  local r = robot.detect()
  robot.turnLeft()
  return { f, l, b, r }
end

--
-- Takes obstruction, turns it into a count of how open the area is
--
local function flbrToOpen(flbr)
  local open = 0
  for i, v in pairs(flbr) do
    if not v then
      open = open + 1
    end
  end
  return open
end

--
-- Directional digging
--
local function swingZ(z)
  if z > 0 then
    return robot.swingUp()
  end
  return robot.swingDown()
end

--
-- Directional moving
--
local function moveZ(z)
  if z > 0 then
    return robot.up()
  end
  return robot.down()
end

--
-- Directional detection
--
local function detectZ(z)
  if z > 0 then
    return robot.detectUp()
  end
  return robot.detectDown()
end

--
-- Complex try digging up or down
--
local SUCCESS = 1
local FAILURE = 0
local WAIT = -1
local function move(dir)
  local blocked, celltype = detectZ(dir)
  if celltype == "entity" then
    return WAIT
  end
  if celltype == "solid" then
    swingZ(dir)
  end
  if moveZ(dir) then
    current_depth = current_depth + dir
    return SUCCESS
  else
    return FAILURE
  end
end

--
-- End state
-- Returns non-function to end the state machine
--
local function End()
  log:write("Halt")
  music.play("%D1,%O6,a,a,a,a,a,a,a,a")
  -- Returns non-function to end the state machine
end

--
-- Move up state
--
local function MoveUp()
  if current_depth >= surface_depth then
    log:write("surface @ " .. current_depth)
    return End
  end
  local result = move(1)
  if result == FAILURE then
    return End
  elseif result == WAIT then
    os.sleep(1)
  end
  return MoveUp
end

--
-- Move down state
--
local function MoveDown()
  if current_depth <= max_depth then
    log:write("max_depth @ " .. current_depth)
    return MoveUp
  end
  if computer.energy() <= min_energy then
    log:write("min_energy @ " .. current_depth)
    return MoveUp
  end
  local new_scan = flbrToOpen(scan())
  if new_scan ~= last_scan then
    log:write("open @ " .. current_depth .. " = " .. new_scan)
    last_scan = new_scan
  end
  local result = move(-1)
  if result == FAILURE then
    log:write("blocked_down @ " .. current_depth)
    return MoveUp
  elseif result == WAIT then
    log:write("waiting_down @ " .. current_depth)
    os.sleep(1)
  end
  return MoveDown
end

--
-- State machine runner
--
local function run(state)
  while type(state) == "function" do
    state = state()
  end
end

--
-- GO!
--
init()
run(MoveDown)